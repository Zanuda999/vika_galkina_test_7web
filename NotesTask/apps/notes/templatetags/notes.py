from django import template
from NotesTask.apps.notes.models import Note


__author__ = 'Vika'

register = template.Library()


@register.simple_tag(name=u'note')
def get_note(note_id):
    try:
        note = Note.objects.get(pk=note_id)
    except Note.DoesNotExist:
        return ''
    return (u'''
    <div class="hero-unit span6 offset3 margin-top">
        <div class="row">
            <div class="span2">
                {date_published}
            </div>
            <div class="span4">
                {content}
            </div>
        </div>
        <div class="row" >
            <div class="span2 offset4">
                {author}
            </div>
        </div>
    </div>'''.format(date_published=note.date_published, content=note.content, author=note.author))
