from django.forms import ModelForm, CharField, TextInput, Textarea
from NotesTask.apps.notes.models import Note

__author__ = 'Vika'


class NoteForm(ModelForm):

    content = CharField(
        min_length=10,
        widget=Textarea(attrs={'class': 'form-control', 'rows': "3", 'cols': '120'}),
        error_messages={'min_length': u"Note text can't be shorter that 10 symbols"}
    )
    author = CharField(widget=TextInput(attrs={'class': 'form-control'}), required=False)

    class Meta:
        model = Note
        fields = ['content', 'author']