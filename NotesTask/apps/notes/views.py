from django.views.generic import ListView
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from NotesTask.apps.notes.models import Note
from NotesTask.apps.notes.forms import NoteForm


class NoteView(ListView):
    form_class = NoteForm
    template_name = 'notes.html'
    model = Note
    context_object_name = 'all_notes'

    def get_context_data(self, **kwargs):
        context = super(NoteView, self).get_context_data(**kwargs)
        context['form'] = self.form_class
        return context

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('index'))
        else:
            return render(request,
                          self.template_name,
                          {self.context_object_name: self.get_queryset(),
                           'form': form})
