from django.core.urlresolvers import reverse
from django_webtest import WebTest
from NotesTask.apps.notes.models import Note
from NotesTask.apps.notes.templatetags.notes import get_note


class AddNoteTestCase(WebTest):

    def setUp(self):
        self.tag_template = u'''
        <div class="hero-unit span6 offset3 margin-top">
            <div class="row">
                <div class="span2">
                    {date_published}
                </div>
                <div class="span4">
                    {content}
                </div>
            </div>
            <div class="row" >
                <div class="span2 offset4">
                    {author}
                </div>
            </div>
        </div>'''
        
    def test_add_note_form_exist(self):
        """
        check that page contain only one form for note
        """

        try:
            self.app.get(reverse('index')).form
        except TypeError:
            self.fail('must be only one form in page')

    def test_uncorrect_data_in_form(self):
        """
        check that form show error message when we post uncorrect note content
        """
        form = self.app.get(reverse('index')).form
        form['author'] = ''  # correct
        form['content'] = 'q'  # uncorrect
        response = form.submit()
        self.assertContains(response, u"Note text can&#39;t be shorter that 10 symbols")

    def test_correct_adding_new_note(self):
        """
        check that new note will be added to db succesfully
        """
        old_all_notes_count = Note.objects.count()
        form = self.app.get(reverse('index')).form
        form['author'] = 'Dr.House'
        form['content'] = 'Everybody lies'
        form.submit().follow()
        new_all_notes_count = Note.objects.count()
        self.assertEqual(old_all_notes_count+1, new_all_notes_count)

    def test_adding_note_to_db(self):
        """
        check that last note in db equal to note that was inputed
        """
        author = 'noname'
        content = 'abrakadabra'
        form = self.app.get(reverse('index')).form
        form['author'] = author
        form['content'] = content
        form.submit().follow()
        last_note = Note.objects.filter(author=author, content=content).order_by('-date_published')[0]
        self.assertEqual(last_note.author, author)
        self.assertEqual(last_note.content, content)


    def test_function_result_get_note(self):
        """
        checks that function return correct answer
        this test need to be at least one Note object in db
        """
        note = Note.objects.first()
        custom_tag_html = self.tag_template.format(
            date_published=note.date_published,
            content=note.content,
            author=note.author
        )
        self.assertInHTML(custom_tag_html, get_note(note.pk))

    def test_custom_tag_displayed_correctly(self):
        """
        check that custom tag displayed correctly
        this test need to be at least one Note object in db
        """
        note = Note.objects.first()
        custom_tag_html = self.tag_template.format(
            date_published=note.date_published,
            content=note.content,
            author=note.author
        )
        response = self.client.get(reverse('index'))
        self.assertInHTML(custom_tag_html, response.content)


    def test_index(self):
        """
                test status code for index page
        """
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)

    def test_notes_count(self):
        """
            checks all the records displayed
        """
        response = self.client.get(reverse('index'))
        self.assertTrue('all_notes' in response.context)
        all_notes_count = Note.objects.count()
        all_notes_from_context = len(response.context['all_notes'])
        self.assertEqual(all_notes_count, all_notes_from_context)