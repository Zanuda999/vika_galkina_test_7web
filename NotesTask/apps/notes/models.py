from django.db import models


class Note(models.Model):
    content = models.CharField(max_length=200)
    author = models.CharField(max_length=50)
    date_published = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.author



