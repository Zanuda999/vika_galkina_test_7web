from django.conf.urls import patterns, include, url
from django.contrib import admin
from NotesTask.apps.notes.views import NoteView

admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', NoteView.as_view(),name='index'),
    url(r'^admin/', include(admin.site.urls)),
)
